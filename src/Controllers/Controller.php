<?php

namespace Component\Controllers;

use Plenty\Plugin\Controller as BaseController;
use Plenty\Plugin\Templates\Twig;

class Controller extends BaseController {

	public function main(Twig $twig)
	{
		return $twig->render('Component::content.component');
	}

}