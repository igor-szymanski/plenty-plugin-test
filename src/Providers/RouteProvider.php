<?php
/**
 * Created by PhpStorm.
 * User: igorszymanski
 * Date: 11.01.18
 * Time: 15:20
 */

namespace Component\Providers;


use Plenty\Plugin\RouteServiceProvider;
use Plenty\Plugin\Routing\Router;

class RouteProvider extends RouteServiceProvider
{
	public function map(Router $router)
	{
		$router->get('component','Component\Controllers\Controller@main');
	}
}