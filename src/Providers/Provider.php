<?php

namespace Component\Providers;

use Plenty\Plugin\ServiceProvider;

class Provider extends ServiceProvider {

	public function register()
	{
		$this->getApplication()->register(RouteProvider::class);
	}

}